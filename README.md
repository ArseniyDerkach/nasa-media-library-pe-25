# Nasa search library

Є апі для фотографій, які робили NASA - <https://images.nasa.gov/docs/images.nasa.gov_api_docs.pdf>
За допомогою цієї апішки треба створити застосунок, який буде показувати користувачу його пошуковий запит
Вимоги для застосунку:

- [ ] Потрібно авторизувати або зареєструвати користувача
- [ ] Коли користувач авторизований є сторінка пошуку - тільки один інпут для пошуку
- [ ] Коли користувач шукає, то показати всі результати на цієї ж сторінці(картинку+заголовок)
- [ ] Кожен результат повинен бути клікабельним, щоб можна було перейти на сторінку результату
- [ ] На сторінці результату повинно бути: картинка, заголовок, фотограф якщо він є, опис, ключові слова
- [ ] На сторінці результату є кнопка Back для повернення до головної сторінки
- [ ] На сторінці результату є кнопка Add to favourites для збереження результату


## Пакети які будемо використовувати:

- [ ] redux-toolkit
- [ ] react-router-dom
- [ ] formik, yup
- [ ] material ui