import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Auth from './pages/Auth/Auth';
import Home from './pages/Home/Home';
import SingleResult from './pages/SingleResult/SingleResult';
import PrivateRoutes from './utils/PrivateRoutes';
import User from './pages/User/User';

export default function AppRoutes() {
  return (
    <Router>
    <Routes>
        <Route path="/auth" element={<Auth />} />
        <Route element={<PrivateRoutes />}>
          <Route path="/" element={<Home />} />
          <Route path="/result/:nasaId" element={<SingleResult />} />
          <Route path="user" element={<User />} />
        </Route>
      </Routes>
    </Router>
  )
}