import AppRoutes from "./AppRoutes"
import {useEffect} from 'react'

function App() {

  useEffect(() => {
    // fetch('https://nasa-media-libarary-fe25-be.vercel.app/').then(res => res.json()).then(data => console.log(data));

    // const user = {
    //   "id": 126,
    //   "firstName": "Arsenii",
    //   "lastName": "Derkach",
    //   "position": "Software engineer"
    // }

    // fetch('https://nasa-media-libarary-fe25-be.vercel.app/user/126').then(res => res.json()).then(data => console.log(data));
    // fetch('https://nasa-media-libarary-fe25-be.vercel.app/user', {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify(user)
    // }).then(res => res.json()).then(data => console.log('added', data.id))
  }, []);

  return (
    <>
      <AppRoutes />
    </>
  )
}

export default App
