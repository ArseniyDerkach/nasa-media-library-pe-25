import { configureStore } from '@reduxjs/toolkit';
import { imagesApi } from './api.slice';


export const store = configureStore({
  reducer: {
    [imagesApi.reducerPath]: imagesApi.reducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(imagesApi.middleware)
})