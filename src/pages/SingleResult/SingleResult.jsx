import { useParams } from "react-router-dom";
import { useGetImageDataQuery, useGetImageQuery } from "../../redux/api.slice"
import Box from '@mui/material/Box';
import Typography from "@mui/material/Typography";

export default function SingleResult() {

  const { nasaId } = useParams();

  const { data } = useGetImageDataQuery(nasaId);
  const { data: imagesCollection } = useGetImageQuery(nasaId);

  return (
    <Box>
      {data && <Typography variant="h5">{data.collection.items[0].data[0].title}</Typography>}
      { (data && data.collection.items[0].data[0].photographer || data.collection.items[0].data[0].secondary_creator ) &&
        <Typography variant="h5">{data.collection.items[0].data[0].photographer || data.collection.items[0].data[0].secondary_creator}</Typography>}
      {imagesCollection && <Box
        component="img"
        sx={{ maxWidth: '1000%' }}
        src={imagesCollection.collection.items[1].href}

      ></Box>}
      
    </Box>
  )
}