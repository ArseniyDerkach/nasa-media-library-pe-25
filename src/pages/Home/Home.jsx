import { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Search from '../../components/Search/Search';
import { useSearchQuery } from '../../redux/api.slice';
import { skipToken } from '@reduxjs/toolkit/query/react';
import SearchResults from '../../components/SearchResults/SearchResults';

export default function Home() {
  const [searchQuery, setSearchQuery] = useState(skipToken);
  const [favs, setFavs] = useState(null);

  useEffect(() => {
    fetch('https://nasa-media-libarary-fe25-be.vercel.app/favs', {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    }).then(res => res.json()).then(data => {
      console.log(data.message);
      setFavs(data.favs);
    })
  }, []);
  const { data } = useSearchQuery(searchQuery)

  function handleSearch(searchValue) {
    setSearchQuery(searchValue);
  }

  return (
    <Box component="section">
      <Search handleSearch={handleSearch} />
      {data && (<SearchResults data={data} favs={favs} />)}
    </Box>
  )
}