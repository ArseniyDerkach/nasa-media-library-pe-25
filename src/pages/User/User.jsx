import { useEffect, useState } from "react"

export default function User() {
  const [name, setName] = useState(null);
  useEffect(() => {
    fetch(
      'https://nasa-media-libarary-fe25-be.vercel.app/user',
      {
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }
    ).then(res => res.json()).then(data => data.name ? setName(data.name): setName(''));
  })
  return (
    <>
      <h4>User info</h4>
      <p>name: { name }</p>
    </>
  )
}