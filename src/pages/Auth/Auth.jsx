import { useState } from "react";
import LoginForm from "../../components/LoginForm/LoginForm";
import SignupForm from "../../components/SignupForm/SignupForm";

export default function Auth() {
  const [currentForm, setCurrentForm] = useState('login');
  return (
    <>
      {currentForm === 'login' ? <>
        <LoginForm />
        <a href="" onClick={(e) => {
          e.preventDefault();
          setCurrentForm('signup')
        }}>Sign up</a>
      </> : <>
          <SignupForm />
          <a href="" onClick={(e) => {
            e.preventDefault();
            setCurrentForm('login')
          }
          }>return to login</a>
      </>}
      
    </>
  )
}