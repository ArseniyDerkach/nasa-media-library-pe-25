import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { useState } from 'react';

export default function Search({handleSearch}) {
  const [searchValue, setSearchValue] = useState('');

  return (
    <Box sx={{
        display: 'flex',
        gap: '10px'
      }}>
        <TextField label="search" variant="outlined" value={searchValue} onChange={(e) => setSearchValue(e.currentTarget.value)} />
        <Button variant="contained" onClick={() => handleSearch(searchValue)}>Search</Button>
      </Box>
  )
}