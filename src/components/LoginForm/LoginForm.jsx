import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

export default function LoginForm() {
  const navigate = useNavigate();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  function handleLogin() {
    console.log('log in');
    fetch('https://nasa-media-libarary-fe25-be.vercel.app/auth', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({username, password})
    }).then(res => res.json())
      .then(data => {
        console.log('logged in', data.message);
        localStorage.setItem('token', data.token);
        navigate('/');
    })
  }

  return (
    <div>
      <input placeholder="Your name" value={username} type="text" onChange={(e) => setUsername(e.currentTarget.value)} />
      <input placeholder="Your password" value={password} type="password" onChange={(e) => setPassword(e.currentTarget.value)} />
      <button onClick={handleLogin}>Log in</button>
    </div>
  )
}