import { useState } from "react"

export default function SignupForm() {

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  function handleSignup() {
    fetch('https://nasa-media-libarary-fe25-be.vercel.app/signup', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ username, password })
    })
      .then(res => res.json())
      .then(data => console.log(data));
  }

  return (
    <>
      <h4>Sign up</h4>
      <input type="text" placeholder="user name" value={username}  onChange={(e)=> setUsername(e.currentTarget.value)}/>
      <input type="password" placeholder="password" value={password} onChange={(e) => setPassword(e.currentTarget.value)} />
      <button onClick={handleSignup}>Sign up</button>
    </>
  )
}