import { useState } from 'react';
import { Link } from 'react-router-dom';
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import StarIcon from '@mui/icons-material/Star';

export default function SearchResult({ item, inFavs }) {

  const [isFav, setIsFav] = useState(inFavs);

  function addToFavourite() {
    console.log('added nasa_id ', item.data[0].nasa_id);
    fetch('https://nasa-media-libarary-fe25-be.vercel.app/addToFav', {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({nasaId: item.data[0].nasa_id})
    }).then(res => res.json())
      .then(data => {
        console.log(data.message);
        setIsFav(true);
    })
  }

  function removeFromFavourite() {
    fetch('https://nasa-media-libarary-fe25-be.vercel.app/removeFromFav', {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({nasaId: item.data[0].nasa_id})
    }).then(res => res.json())
      .then(data => {
        console.log(data.message);
        setIsFav(false);
    })
  }

  // console.log(item);
  return (
    <Box sx={{ position: 'relative' }}>
      <Box sx={{position: 'absolute', right: '5px', top: '5px'}}>
          {isFav ? <StarIcon color='warning' onClick={removeFromFavourite} /> : <StarBorderIcon color='warning' onClick={addToFavourite} />}
        </Box>
      <Link to={`/result/${item.data[0].nasa_id}`}>
        <Box
          component="img"
          sx={{
            maxWidth: '100%'
          }}
          src={item.links[0].href}
          alt={item.data[0].description}
        ></Box>
        <Typography>{item.data[0].title}</Typography>
      </Link>
    </Box>
  )
}